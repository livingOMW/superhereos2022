package proyecto.superheroes.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import proyecto.superheroes.backend.entity.TipoPersonaje;

public interface TipoPersonajeRepository extends JpaRepository<TipoPersonaje, Integer> {

	TipoPersonaje findByDescripcion(String descripcion);
}
