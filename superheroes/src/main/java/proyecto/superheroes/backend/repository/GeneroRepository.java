package proyecto.superheroes.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import proyecto.superheroes.backend.entity.Genero;

public interface GeneroRepository extends JpaRepository<Genero, Integer> {

	Genero findByNombre(String nombre);
}
