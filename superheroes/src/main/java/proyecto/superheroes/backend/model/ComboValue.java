package proyecto.superheroes.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ComboValue {
    private Object codigo;
    private String nombre;
}
