package proyecto.superheroes.backend.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import proyecto.superheroes.backend.entity.Genero;
import proyecto.superheroes.backend.entity.TipoPersonaje;
import proyecto.superheroes.backend.model.ComboValue;
import proyecto.superheroes.backend.repository.GeneroRepository;
import proyecto.superheroes.backend.repository.TipoPersonajeRepository;

@Service
@Transactional(readOnly = true)
public class ComboService {
	
	@Autowired
	private GeneroRepository generoRepository;
	
	@Autowired
	private TipoPersonajeRepository tipoPersonajeRepository;
	
	/**
	 * @return devuelve combo con todos los tipos de genero
	 */
	public List<ComboValue> getGeneroCombo() {
		List<Genero> generoList = generoRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		if (!generoList.isEmpty()) {
			return generoList.stream()
					.map(genero -> new ComboValue(genero.getId(), genero.getNombre()))
					.collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
	
	/**
	 * @return devuelve un combo con todos los tipos de personajes existentes
	 */
	public List<ComboValue> getTipoPersonajeCombo(){
		List<TipoPersonaje> tipoList = tipoPersonajeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		if (!tipoList.isEmpty()) {
			return tipoList.stream()
					.map(tipo -> new ComboValue(tipo.getId(), tipo.getDescripcion()))
					.collect(Collectors.toList());
		}
		return new ArrayList<>();
	}
}
