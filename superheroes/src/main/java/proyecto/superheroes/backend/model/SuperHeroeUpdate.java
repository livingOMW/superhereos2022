package proyecto.superheroes.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SuperHeroeUpdate {
	
	private Integer id;
	private String nombre;
	private Integer tipoPersonajeId;
	private Integer generoId;
}
