package proyecto.superheroes.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import proyecto.superheroes.backend.model.ComboValue;
import proyecto.superheroes.backend.services.ComboService;

@RestController
@RequestMapping("/combos")
public class ComboController {
	
	@Autowired
	private ComboService comboService;
	
	@GetMapping("/getTiposPersonaje")
	public ResponseEntity<List<ComboValue>> getTipoPersonaje() {
		return ResponseEntity.ok(comboService.getTipoPersonajeCombo());
	}
	
	@GetMapping("/getGeneros")
	public ResponseEntity<List<ComboValue>> getGeneros() {
		return ResponseEntity.ok(comboService.getGeneroCombo());
	}
}
