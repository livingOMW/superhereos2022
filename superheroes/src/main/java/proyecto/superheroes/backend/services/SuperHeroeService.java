package proyecto.superheroes.backend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import proyecto.superheroes.backend.aop.LogExecutionTime;
import proyecto.superheroes.backend.entity.Genero;
import proyecto.superheroes.backend.entity.SuperHeroe;
import proyecto.superheroes.backend.entity.TipoPersonaje;
import proyecto.superheroes.backend.mapper.SuperHeroeMapper;
import proyecto.superheroes.backend.model.SuperHeroeDTO;
import proyecto.superheroes.backend.model.SuperHeroeUpdate;
import proyecto.superheroes.backend.repository.SuperHeroeRepository;

@Service
@Transactional(readOnly = true)
public class SuperHeroeService {
	
	@Autowired
	private SuperHeroeRepository superHeroeRepository;
	
	@Autowired
	private SuperHeroeMapper superHeroeMapper;
	
	/**
	 * 
	 * @return deveuvle a todos los super heroes de BD
	 */
	@LogExecutionTime
	public List<SuperHeroeDTO> getAllSuperHeroes(){
		return superHeroeMapper.toSuperHeroe(superHeroeRepository.findAll());
	}
	
	/**
	 * 
	 * @param id del super hereo
	 * @return devuelve un super heroe
	 */
	public SuperHeroeDTO getSuperHeroeById(Integer id){
		return superHeroeMapper.toSuperHeroe(superHeroeRepository.findById(id).orElse(null));
	}
	
	/**
	 * @descripcion metodo que busca cualquier coincidencia del param en el nombre del super heroe es no case sensitive
	 * @param param 
	 * @return lista de super heroes
	 */
	public List<SuperHeroeDTO> getAllSuperHeroesByParam(String param) {
		return superHeroeRepository.getSuperHeroesByParam(param);
	}
	
	/**
	 * @descripcion metodo que elimina un super heroe por su id
	 * @param id del super heroe
	 * @return true false si se ha eliminado
	 */
	@Transactional()
	public boolean deleteByParam(Integer id) {
		boolean eliminado = true;
		superHeroeRepository.deleteById(id);
		if(superHeroeRepository.findById(id).isPresent()) {
			eliminado=false;
		}
		return eliminado;
	}
	
	/**
	 * @descripcion metodo que actualiza un super heroe
	 * @param superHeroeUpdate objeto que viene de pantalla para actualiza un superHeroe
	 * @return true o false si se ha actualizado o no 
	 */
	@Transactional()
	public boolean updateSuperHeroe(SuperHeroeUpdate superHeroeUpdate) {
		boolean actualizado = true;
		SuperHeroe superHereoBD = superHeroeRepository.findById(superHeroeUpdate.getId()).orElse(null);
		if(!ObjectUtils.isEmpty(superHereoBD)) {
			superHereoBD.setGenero(new Genero(superHeroeUpdate.getGeneroId()));
			superHereoBD.setNombre(superHeroeUpdate.getNombre());
			superHereoBD.setTipoPersonaje(new TipoPersonaje(superHeroeUpdate.getTipoPersonajeId()));
			superHeroeRepository.save(superHereoBD);
		}
		return actualizado;
	}
	
}
