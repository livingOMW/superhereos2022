package proyecto.superheroes.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import proyecto.superheroes.backend.entity.SuperHeroe;

public interface SuperHeroeRepository extends JpaRepository<SuperHeroe, Integer>, SuperHereoCustomRepository {

}
