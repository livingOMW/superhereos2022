package proyecto.superheroes.backend.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import proyecto.superheroes.backend.entity.SuperHeroe;
import proyecto.superheroes.backend.model.SuperHeroeDTO;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface SuperHeroeMapper {
	
	@Mapping(target = "tipoPersonaje", source = "superHeroe.tipoPersonaje.descripcion")
	@Mapping(target = "genero", source = "superHeroe.genero.nombre")
	SuperHeroeDTO toSuperHeroe(SuperHeroe superHeroe);
	List<SuperHeroeDTO> toSuperHeroe(List<SuperHeroe> listaSuperHeroes);
}
