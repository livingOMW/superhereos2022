package proyecto.superheroes.backend.persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import proyecto.superheroes.backend.entity.Genero;
import proyecto.superheroes.backend.entity.SuperHeroe;
import proyecto.superheroes.backend.entity.TipoPersonaje;
import proyecto.superheroes.backend.repository.GeneroRepository;
import proyecto.superheroes.backend.repository.SuperHeroeRepository;
import proyecto.superheroes.backend.repository.TipoPersonajeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
class SpringBootJPAIntegrationTest {

	@Autowired
	private SuperHeroeRepository superHeroeRepository;
	
	@Autowired
	private TipoPersonajeRepository tipoPersonajeRepository;
	
	@Autowired
	private GeneroRepository generoRepository;
	
    @Test
    void superHeroeEntityIntegrationTest() {
    	SuperHeroe superHeroeNuevo = superHeroeRepository.save(
    			new SuperHeroe(13,"Aquaman",new TipoPersonaje(1),new Genero(1)));
    	SuperHeroe superHeroeBD = superHeroeRepository.findById(superHeroeNuevo.getId()).orElse(null);
 
        assertNotNull(superHeroeBD);
        assertEquals(superHeroeNuevo.getId(), superHeroeBD.getId());
    } 
    
    @Test
    void tipoPersonajeEntityIntegrationTest() {
    	TipoPersonaje tipoNuevo = tipoPersonajeRepository.save(new TipoPersonaje("Neutral"));
    	TipoPersonaje tipoBD = tipoPersonajeRepository.findByDescripcion("Neutral");
    	 assertNotNull(tipoBD);
    	 assertEquals(tipoNuevo.getId(),tipoBD.getId());
    }
    
    @Test
    void generoEntityIntegrationTest() {
    	Genero generoNuevo = generoRepository.save(new Genero("Desconocido"));
    	Genero generoBD = generoRepository.findByNombre("Desconocido");
    	assertNotNull(generoBD);
    	assertEquals(generoNuevo.getId(),generoBD.getId());
    }
    
}
