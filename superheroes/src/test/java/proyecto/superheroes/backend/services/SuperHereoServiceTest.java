package proyecto.superheroes.backend.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class SuperHereoServiceTest {
	
	@Autowired
	private SuperHeroeService superHeroeService;
	
    @Test
    void buscarPorIdTest() {
    	assertEquals("Hulk", superHeroeService.getSuperHeroeById(3).getNombre());
    }

}
