package proyecto.superheroes.backend.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SUPER_HEROE")
public class SuperHeroe implements Serializable{
	
	private static final long serialVersionUID = 7297093927025037981L;

	@Id
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "NOMBRE", nullable = false, length = 250)
	private String nombre;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TIPO_PERSONAJE_ID")
	private TipoPersonaje tipoPersonaje;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GENERO_ID")
	private Genero genero;
}
