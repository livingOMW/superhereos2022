package proyecto.superheroes.backend.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import proyecto.superheroes.backend.model.SuperHeroeDTO;
import proyecto.superheroes.backend.repository.SuperHereoCustomRepository;

public class SuperHereoCustomRepositoryImpl implements SuperHereoCustomRepository {
	
    @PersistenceContext
    private EntityManager entityManager;
    
    public List<SuperHeroeDTO> getSuperHeroesByParam(String param){
    	StringBuilder querySB = new StringBuilder();
    	querySB.append(" SELECT new proyecto.superheroes.backend.model.SuperHeroeDTO (");
    	querySB.append(" s.id,");
    	querySB.append(" s.nombre,");
    	querySB.append(" t.descripcion,");
    	querySB.append(" g.nombre )");
    	querySB.append(" FROM SuperHeroe s"
    				 + " LEFT JOIN s.genero g "
    				 + " LEFT JOIN s.tipoPersonaje t");
    	if(!param.isEmpty()) {
    		querySB.append(" WHERE UPPER(s.nombre) LIKE :param");
    	}
		TypedQuery<SuperHeroeDTO> query = entityManager.createQuery(querySB.toString(), SuperHeroeDTO.class);
      	if(!param.isEmpty()) {
      		query.setParameter("param", "%"+param.toUpperCase()+"%");
    	}
      	return query.getResultList();
		
    }
}
