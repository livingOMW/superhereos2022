package proyecto.superheroes.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
		         .select()
		         .apis(RequestHandlerSelectors.basePackage("proyecto.superheroes.backend.controller"))
		         .build()
		         .apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder().title("API superheroes").version("1.0.0")
	            .description("superheroes marvel")
	            .contact(new Contact("Oleg Matunin","www.example.com","met1810@gmail.com"))
	            .build();
	}

}
