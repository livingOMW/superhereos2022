package proyecto.superheroes.backend.repository;

import java.util.List;

import proyecto.superheroes.backend.model.SuperHeroeDTO;

public interface SuperHereoCustomRepository {

	List<SuperHeroeDTO> getSuperHeroesByParam(String param);
}
